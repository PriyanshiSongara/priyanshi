# -*- coding: utf-8 -*-
"""
Created on Thu Feb 21 12:59:33 2019

@author: sujit
"""

import yaml
import sys
import logging
import bcrypt
import time
import boto3
import string
import datetime
import pprint
from flask import Flask, redirect, url_for, session, request, jsonify
from flask_oauthlib.client import OAuth
from flask_restful import Api
from util import *

from google.oauth2 import id_token
from google.auth.transport import requests

# TODO:
# =====
# - Create users logged in through google oauth. Might have to maintain google id and type of user (local, google, facebook)
# - Create JWT for google users during authenticate API (login)
# - Create email validation check API. input = {email, phone, otp} --> Detail out the business logic in this API
# - Handle logout
#     Option-1: Delete JWT from client (browser/mobile app) --> No change in Backend for this option.
#     Option-2: Option-1 + Add JWT into blacklist of JWTs along with TTL + Check against the JWT blacklist for each API request
#     Option-3: Option-2 + Refresh Token
# - Integrate Swagger
# - Remove redundant variables
# - Handle DB reconnection
# - Move all the code in global scope to lambda_handler
# - Support WSGI
#     * Apache + mod_wsgi
#     * Nginx  + uWSGI
#     * Try Bjoern (may not work with Flask. So try Bottle or Falcon)
#     * Nginx  + Gunicorn  --> Go for this. And ignore the others for now

print("Starting User-Management API Service...")

# Initialize Flask App
app = Flask(__name__)
api = Api(app)
app.debug = True
app.secret_key = 'development'
oauth = OAuth(app)

# Read config details
cfg = None
with open("config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile)
pprint.pprint(cfg)

# Initialize global variables
rds_host  = cfg['db']['host']
name = cfg['db']['username']
password = cfg['db']['password']
db_name = cfg['db']['schema']
port = cfg['db']['port']
conn = None

# Initialize google oauth
google = oauth.remote_app(
    'google',
    consumer_key = cfg['google']['id'],
    consumer_secret = cfg['google']['secret'],
    request_token_params={
        'scope': 'email profile'
    },
    base_url='https://www.googleapis.com/oauth2/v1/',
    request_token_url=None,
    access_token_method='POST',
    access_token_url='https://accounts.google.com/o/oauth2/token',
    authorize_url='https://accounts.google.com/o/oauth2/auth',
)

# =========================== General APIs ============================
@app.route('/')
def index():
    # Remove this. Should be ideally done only during log out
    session.clear()
    if 'google_token' in session:
        me = google.get('userinfo')
        if('error' not in me.data):
            return jsonify({"data": me.data})
    return redirect(url_for('login_using_google'))


@app.route('/health', methods=['GET'])
def health_request():
    return "Success"

    
# ============================= Local User ==============================
@app.route('/register/user', methods=['POST'])
def registration_request():
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    global conn
    # If conn is not valid, reconnect again
    if(conn is None):
        conn = get_db_connection()
    logger.info("Connection created")
    data = request.json 
    
    # Check if user already exists?
    cursor = conn.cursor()
    sql = "SELECT * FROM users_onboarding"
    logger.info(sql)
    cursor.execute(sql)
    user_email = data['email']
    for row in cursor:
            if(row[1] == user_email):
                logger.info(user_email+" already exist")
                return user_email+" already exists"
        
    # Encrypt password using bcrypt(password, salt, hash)
    encoded_pwd = data['password'].encode('UTF_8')
    print(encoded_pwd)
    
    print(type(bcrypt.gensalt()))
    hashed_passwd = bcrypt.hashpw(encoded_pwd, bcrypt.gensalt())
    print("Hash pwd: "+str(hashed_passwd))
    # Insert new potential user into users_onboarding table
    logger.info("Inserting data to DB")
    sql =   "Insert into users_onboarding (email, fn, ln, phone, password,is_validated) values (%s, %s, %s, %s, %s, %s) "
    cursor.execute(sql, (user_email, data['fname'],data['lname'],data['phno'],hashed_passwd,0))
    conn.commit()
    last_rowid = cursor.lastrowid
    
    # OTP in otp_table (6 digit random number and unique to the phone number)
    otp = generateOTP(4)
    sql = "Insert into otp_validation(users_onboarding_id,otp,expiry_time) values(%s,%s,%s)"
    cursor.execute(sql,(last_rowid,otp,int(time.time()+120)))
    conn.commit()
    # Send OTP over SMS
    '''
    client = boto3.client(
    "sns",
    aws_access_key_id='AKIAIF47SFC737HJQKDQ',
    aws_secret_access_key='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',
    region_name="eu-west-1"
    )
    client.publish(
    PhoneNumber="+91"+(str(data['phno'])),
    Message=otp
    )
    '''

    #Create unique email validation token Hash(email, phone, secret, random number)
    token1 = generate_email_token(32)
    print(token1)
    token2 = generate_email_token(32)
    print(token2)
    url = "http://localhost:5002?email="+user_email+",token1="+token1+",token2="+token2
    print(url)
    #Insert email validation token into email_validation_token table. Have 1 day as expiry date
    sql = "Insert into email_validation(users_onboarding_id, token_one, token_two, expiry_time) values(%s,%s,%s,%s)"
    cursor.execute(sql,(last_rowid,token1,token2,int(time.time()+86400)))
    conn.commit()
    #Send validation email
    #email_token_for_verification(user_email,url) 
    return "Success"

# OTP validation check API. input = {email, phone, otp} --> Detail out the business logic in this API
@app.route('/register/otp_validate', methods=['POST'])
def validate_otp():
    global conn
    if(conn is None):
        conn = get_db_connection()
    data = request.json
    print("Executing query")
    
    sql = "SELECT * FROM otp_validation where users_onboarding_id = (select id from users_onboarding where email = %s)"
    cursor = conn.cursor()
    cursor.execute(sql,(data['email']))
    for row in cursor:
        print(row[1])
        if(row[2] == data['otp'] and time.time() < float(row[3])):
            print("OTP validated")
            sql = "update users_onboarding set is_validated = 1 where email = %s"
            cursor = conn.cursor()
            cursor.execute(sql,data['email'])
            conn.commit()
            print("users_onboarding updated")
            sql = "SELECT * FROM users_onboarding where email = %s"
            cursor.execute(sql,(data['email']))
            for row in cursor:
                print(row)
                print(row[1])
                sql = "insert into users(user_name,first_name, last_name,phone, inactive) values (%s,%s,%s,%s,%s)";
                cursor = conn.cursor()
                cursor.execute(sql,(row[1],row[2],row[3],row[4],0))
                conn.commit()
                print("inserted into users")
                last_rowid = cursor.lastrowid
                sql = "insert into users_logins(user_id,password) values(%s,%s)"
                cursor = conn.cursor()
                cursor.execute(sql,(last_rowid,row[5]))
                conn.commit()
                print("inserted into users_logins")
            return "OTP validation success"
        return "OTP validation Failed"
       
@app.route('/authenticate/user', methods=['POST'])
def login_validation():
    data = request.json
    sql = "select * from users where user_name = %s"
    cursor = conn.cursor()
    cursor.execute(sql,(data['username']))
    print (cursor.rowcount)
    if(cursor.rowcount<1):
        return "User not found"
    for row in cursor:
        sql = "select * from users_logins where user_id=%s"
        cursor1 = conn.cursor()
        cursor1.execute(sql,(row[0]))
        for row in cursor1:
            print(row[1])
            password = data['passcode'].encode('UTF_8')
            print(password)
            if bcrypt.checkpw(password, row[1].encode('UTF_8')):
                auth_token = encode_auth_token(data['username'])
                print(auth_token)
                responseObject = {
                      'status': 'success',
                      'message': 'Successfully logged in.',
                      'auth_token': auth_token
                    }
                return str(responseObject)
    return "User validation failed"


@app.route('/register/email_validate', methods=['POST']) 
def validate_email():
    global conn
    if(conn is None):
        conn = get_db_connection()
    email = request.args.get('email')
    token1 = request.args.get('token1')
    token2 = request.args.get('token2')
    print("selecting data from email validation")
    sql = "SELECT * FROM email_validation where users_onboarding_id = (select id from users_onboarding where email = %s)"
    cursor = conn.cursor()
    cursor.execute(sql, (email))
    for row in cursor:
        print(row)
        if(row[2] == token1 and row[3] == token2 and time.time < float(row[4])):
            print("Email validated")
            sql = "update users_onboarding set is_validated = 1 where email = %s"
            cursor = conn.cursor()
            cursor.execute(sql, email)
            conn.commit()
            print("users_onboarding updated")
            sql = "SELECT * FROM users_onboarding where email = %s"
            cursor.execute(sql, (email))
            for row in cursor:
                print(row)
                print(row[1])
                sql = "insert into users(user_name,first_name, last_name,phone, inactive) values (%s,%s,%s,%s,%s)";
                cursor = conn.cursor()
                cursor.execute(sql, (row[1], row[2], row[3], row[4], 0))
                conn.commit()
                print("inserted into users")
                last_rowid = cursor.lastrowid
                sql = "insert into users_logins(user_id,password) values(%s,%s)"
                cursor = conn.cursor()
                cursor.execute(sql, (last_rowid, row[5]))
                conn.commit()
                print("inserted into users_logins")
            return "Email validation success"
        return "Email validation Failed"
    
# ============================= Google User =============================
# Examples: https://github.com/lepture/flask-oauthlib/blob/master/example/google.py
#           https://google-auth.readthedocs.io/en/latest/reference/google.oauth2.id_token.html
#           https://github.com/googleapis/google-auth-library-python
@app.route('/authenticate/google_user')
def login_using_google():
    return google.authorize(callback=url_for('authorize_google_user', _external=True))


@app.route('/logout')
def logout():
    session.pop('google_token', None)
    return redirect(url_for('index'))


@app.route('/authorize/google_user')
def authorize_google_user():
    try:
        resp = google.authorized_response()
        token = resp['id_token']
        request = requests.Request()
        id_info = id_token.verify_oauth2_token(token, request, audience=None)
        pprint.pprint(id_info)

        if resp is None:
            return 'Access denied: reason=%s error=%s' % (
                request.args['error_reason'],
                request.args['error_description']
            )
        session['google_token'] = (resp['access_token'], '')

        # Get basic profile info from google for the user using access_token
        me = google.get('userinfo')
        print(me.data['family_name'])
        #Validate the user and add it in users table if doesnt exist. In either of the cases get users.id (primary key)
        print("Connection creation started")
        global conn
        if(conn is None):
          conn = get_db_connection()
        sql = "select * from users where auth_id = %s"
        cursor = conn.cursor()
        cursor.execute(sql,(id_info['sub']))
        print("query executed")
        print (cursor.rowcount)
        if(cursor.rowcount<1):
            print(me.data['email'], me.data['given_name'], me.data['family_name'], 1,id_info['sub'],"Google")
            sql = "insert into users(user_name,first_name,last_name,inactive,auth_id,auth_type) values (%s,%s,%s,%s,%s,%s)";
            cursor = conn.cursor()
            cursor.execute(sql, (me.data['email'], me.data['given_name'], me.data['family_name'], 1,id_info['sub'],"Google"))
            conn.commit()
            print("inserted into users")
        #Generating auth_token and send it in response (user users.id and some more parameters to generate it)
        auth_token = encode_auth_token(me.data['email'])
        responseObject = {
          'status': 'success',
          'message': 'Successfully authenticated by google.',
          'auth_token': auth_token
        }
        #Returning SUCCESS response as appropriate to frontend
        return str(responseObject)
        
    except:
        # TODO: Return ERROR code and make UI to show error and if needed take to login page
        return "FAILURE"


@google.tokengetter
def get_google_oauth_token():
    return session.get('google_token')


# ============================= Feature Specific APIs =============================   
@app.route('/profile', methods=['POST', 'GET'])
def get_profile_info():
    
    return "This is a Stub. Yet to develop!"


@app.route('/dashboard', methods=['POST'])
def get_dashboard_details():
    print(request.headers)

    # TODO: Remove this. This is already moved to API GW
    jwt_token = request.headers['Authorization'].split(" ")[1]
    return decode_auth_token(jwt_token)

@app.route('/profileInfo', methods=['POST'])
def get_profile_information():
    data = request.json
    global conn
    if(conn is None):
        conn = get_db_connection()
    sql = "select * from users where user_name = %s"
    cursor = conn.cursor()
    cursor.execute(sql, (data['email']))
    for row in cursor:
        return jsonify({"Profile Info": row})
    return null;

def lambda_handler(event,context):
    global conn
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    try:
        conn = get_db_connection(rds_host, name, password, db_name, port)
        print(conn)
                
    except:
        print('!!!!!!!!!!!!!!!!!')
        logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
        sys.exit()
    logger.info("SUCCESS: Connection to RDS mysql instance succeeded")
    
lambda_handler(None, None) 

if __name__ == '__main__':
   app.run(port = 5002, host = "0.0.0.0") 
