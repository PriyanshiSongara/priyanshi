# -*- coding: utf-8 -*-
"""
Created on Thu Feb 21 12:59:33 2019

@author: sujit
"""

import sys
import pymysql
import random
import math 
import time
import boto3
import string
import jwt
import datetime


def encode_auth_token(user_id):
    """
    Generates the Auth Token
    :return: string
    """
    try:
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1, seconds=5),
            'iat': datetime.datetime.utcnow(),
            'sub': user_id
        }
        print("Payload:", payload)
        #print(jwt.encode(payload,app.config.get('SECRET_KEY'),algorithm='HS256'))
        return jwt.encode(
            payload,
            'SECRET_KEY',
            algorithm='HS256'
        )
    except Exception as e:
        return e

def decode_auth_token(auth_token):
    """
    Validates the auth token
    :param auth_token:
    :return: integer|string
    """
    try:
        payload = jwt.decode(auth_token, 'SECRET_KEY')
        return payload['sub']
    except jwt.ExpiredSignatureError:
        return 'Signature expired. Please log in again.'
    except jwt.InvalidTokenError:
        return 'Invalid token. Please log in again.'

def generateOTP(n): 
    otp = ""
    for i in range(n) : 
        otp += string.digits[math.floor(random.random() * 10)]       
    return otp

def get_db_connection(host, username, password, schema, port):
    return pymysql.connect(host,
                           user=username,
                           passwd=password,
                           db=schema,
                           connect_timeout=10)
def generate_email_token(n):
    token = ""
    for i in range(n) : 
        token += string.digits[math.floor(random.random() * 10)]       
    return token    
    
def email_token_for_verification(email,url):   
    SENDER = "sujit kumar <sujit_kumar.y@yahoo.com>"
    RECIPIENT = "sujit kumar <sujit_kumar.y@yahoo.com>"
    AWS_REGION = "us-east-1"
    SUBJECT = "User Management verification email"
    # The HTML body of the email.
    BODY_HTML = "<html>"
    "<head></head>"
    "<body>"
    "<h1>Email Authentication for User Management</h1>"
    "<p>This email was sent with"
    "<a href='link'>Click Here</a></p>"
    "</body>"
    "</html>"
    BODY_HTML.replace("link",url)
    CHARSET = "UTF-8"
    client = boto3.client('ses',
                          aws_access_key_id='AKIAIF47SFC737HJQKDQ',
                          aws_secret_access_key='prLuIjXHpAylzszeXNEUqLMMBnO6py+nsJV0GBBz',
                          region_name=AWS_REGION)
