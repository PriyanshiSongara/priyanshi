package com.example.user_mgmt;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;








/*@SpringBootApplication
public class UserMgmtV2Application {

	public static void main(String[] args) {
		SpringApplication.run(UserMgmtV2Application.class, args);
		System.out.println("Started");
		String url = "jdbc:mysql://sujith-users.csjwywhrbn2c.us-east-2.rds.amazonaws.com:3306/users_db";
		String userName = "root";
		String password = "Python!2019";
		String dbName = "users_db";
		String driver = "com.mysql.jdbc.Driver";
		try {
			//Connection connection = DriverManager.getConnection(url, userName, password);
			System.out.println(connection);
			Statement stmt = connection.createStatement();
	        ResultSet rs;

	       rs = stmt.executeQuery("SELECT * FROM users_onboarding ");
	       System.out.println("ResultSet"+rs);
	        while ( rs.next() ) {
	            String lastName = rs.getString("Lname");
	            System.out.println(lastName);
	        }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Error creating connection");
		}
	}

}*/
//how to enable discovery client
//hotw to user SpringBoot ServletInitializer

@SpringBootApplication
@EnableOAuth2Sso
public class UserMgmtV2Application extends SpringBootServletInitializer  {

	@Override
	protected SpringApplicationBuilder configure(final SpringApplicationBuilder application) {
		return application.sources(UserMgmtV2Application.class);
	}
	public static void main(final String[] args) {
		SpringApplication.run(UserMgmtV2Application.class, args);
		
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
	    return new BCryptPasswordEncoder();
	}
	
	
	
	
	
	
	


}
