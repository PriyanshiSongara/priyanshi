package com.example.user_mgmt.commons;

public class ApiConstants {

	public static final String USER_MGMT_LOGIN = "/register/user";
	public static final String USER_MGMT_OTP_VALIDATION = "/register/otp_validate";
	public static final String GOOGLE_AUTHENTICATE = "/authenticate/google_user";
	public static final String GOOGLE_USER = "/google/user";
	public static final String USER_MGMT_AUTHENTICATE = "/authenticate/user";
	public static final String JWT_SECRET = "n2r5u8x/A%D*G-KaPdSgVkYp3s6v9y$B&E(H+MbQeThWmZq4t7w!z%C*F-J@NcRf";
}
