package com.example.user_mgmt.commons;

import java.security.Key;
import java.util.Date;
import java.util.Random;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


@Component
public class ServiceHelper {
	

	 
	static String digits = "0123456789";
	public static String generateOtp()
	{ String otp="";
		Random rand=new Random();
		{
			for(int i=0; i<4; i++)
		    {
		        otp =otp + digits.charAt(rand.nextInt(digits.length()));
		        System.out.println(otp);
		    }
		    return otp;
		}
	}
	
	public static String createJWT(String id) {
		  
	 	  SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

	    long nowMillis = System.currentTimeMillis();
	    Date now = new Date(nowMillis);
	    long expireMillis= nowMillis+86400000;
	    Date expire=new Date(expireMillis);

	    //We will sign our JWT with our ApiKey secret
	    byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(ApiConstants.JWT_SECRET);
	    Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

	    //Let's set the JWT Claims
	    JwtBuilder builder = Jwts.builder().setId(id)
	            .setIssuedAt(now)
	            .setSubject(id)
	            .signWith(signatureAlgorithm, signingKey)
	            .setExpiration(expire);
	  
	    
	  
	    //Builds the JWT and serializes it to a compact, URL-safe string
	    return builder.compact();
	}
	
	
}
