package com.example.user_mgmt.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;


@Entity
@Table(schema = "users_db", name = "otp_validation")
@NamedNativeQueries({

    @NamedNativeQuery(
            name = "getOtpValidation",
            query = "select u.id ,u.users_onboarding_id ,u.otp, u.expiry_time"
            + " from users_db.otp_validation u where u.users_onboarding_id = (select v.id from users_db.users_onboarding v where email = :email)",
          

            resultClass = OtpInsertionDto.class) })
public class OtpInsertionDto {
	
	@Id
	@Column(name = "id")
	private int id;
	
	
	@Column(name = "users_onboarding_id")
	private int usersOnboardingId;
	@Column(name = "otp")
	private String otp;
	@Column(name = "expiry_time")
	private String expiryTime;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUsersOnboardingId() {
		return usersOnboardingId;
	}
	public void setUsersOnboardingId(int usersOnboardingId) {
		this.usersOnboardingId = usersOnboardingId;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public String getExpiryTime() {
		return expiryTime;
	}
	public void setExpiryTime(String expiryTime) {
		this.expiryTime = expiryTime;
	}
	

}
