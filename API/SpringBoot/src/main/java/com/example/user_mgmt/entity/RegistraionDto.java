package com.example.user_mgmt.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "users_db", name = "users_onboarding")
public class RegistraionDto {

	 @Id
     @GeneratedValue(strategy=GenerationType.IDENTITY)
     private int id;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "email")
	private String email;
	@Column(name = "fn")
	private String fName;
	@Column(name = "ln")
	private String lName;
	@Column(name = "phone")
	private String phone;
	@Column(name = "password")
	private String password;

	@Column(name = "is_validated")
	private int isValidated;

	public int getIsValidated() {
		return isValidated;
	}

	public void setIsValidated(int isValidated) {
		this.isValidated = isValidated;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "RegistraionDto [email=" + email + ", fName=" + fName + ", lName=" + lName + ", phone=" + phone
				+ ", password=" + password + "]";
	}

}
