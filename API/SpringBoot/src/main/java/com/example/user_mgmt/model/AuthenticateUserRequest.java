package com.example.user_mgmt.model;

public class AuthenticateUserRequest {
	
	private String userName;
	private String passwordEntered;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPasswordEntered() {
		return passwordEntered;
	}

	public void setPasswordEntered(String passwordEntered) {
		this.passwordEntered = passwordEntered;
	}
	

}
