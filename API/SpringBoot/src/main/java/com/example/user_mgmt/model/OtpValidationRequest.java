package com.example.user_mgmt.model;

import org.hibernate.validator.constraints.NotEmpty;

public class OtpValidationRequest {
	@NotEmpty
	private String email;
	
	private String phone;
	private String otp;
	
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}

	

}
