package com.example.user_mgmt.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.user_mgmt.entity.OtpInsertionDto;
import com.example.user_mgmt.entity.RegistraionDto;

@Repository
public interface OtpValidationRepository extends CrudRepository<OtpInsertionDto, Long> {

}
