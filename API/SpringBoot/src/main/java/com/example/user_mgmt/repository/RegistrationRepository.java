package com.example.user_mgmt.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.user_mgmt.entity.RegistraionDto;

@Repository
public interface RegistrationRepository extends CrudRepository<RegistraionDto, Long> {

		
		public RegistraionDto getByEmail(@Param("email") String emailId);

		
		
		
		
		
		
		
}
