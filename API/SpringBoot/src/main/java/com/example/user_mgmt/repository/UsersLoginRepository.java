package com.example.user_mgmt.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.user_mgmt.entity.UsersLoginDto;

@Repository
public interface UsersLoginRepository extends CrudRepository<UsersLoginDto, Long>{
	
	public UsersLoginDto getByUserId(@Param("user_id") int i);

}
