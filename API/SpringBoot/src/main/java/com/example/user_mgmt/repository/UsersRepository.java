package com.example.user_mgmt.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.user_mgmt.entity.UsersDto;

@Repository
public interface UsersRepository extends CrudRepository<UsersDto, Long>{
	
	
	public UsersDto getByUserName(@Param("user_name") String userName);
	
	//public UsersDto getByFirstName(@Param("first_name") String fName,@Param("last_Name") String lName);

}
