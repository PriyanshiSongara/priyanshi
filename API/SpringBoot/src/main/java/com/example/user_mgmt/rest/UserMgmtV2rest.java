package com.example.user_mgmt.rest;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.security.Principal;
import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.user_mgmt.commons.ApiConstants;
import com.example.user_mgmt.model.AuthenticateUserRequest;
import com.example.user_mgmt.model.OtpValidationRequest;
import com.example.user_mgmt.model.OtpValidationResponse;
import com.example.user_mgmt.model.RegistrationRequest;
import com.example.user_mgmt.model.RegistrationResponse;
import com.example.user_mgmt.model.UserAuthenticationResponse;
import com.example.user_mgmt.service.RegistrationService;

import io.swagger.annotations.SwaggerDefinition;

@SwaggerDefinition
@Produces(APPLICATION_JSON_VALUE)
@Consumes(APPLICATION_JSON_VALUE)
@RestController
public class UserMgmtV2rest {
	
	@Value("${security.oauth2.google}")
	private String google;

	@Autowired
	private RegistrationService registrationService;
	

	@RequestMapping(value = ApiConstants.USER_MGMT_LOGIN, method = RequestMethod.POST)
	public @ResponseBody RegistrationResponse doRegistration(@RequestBody RegistrationRequest request1) {
		RegistrationResponse response = new RegistrationResponse();
		response = registrationService.doRegistration(request1);
		return response;

	}
	
	@RequestMapping(value = ApiConstants.USER_MGMT_OTP_VALIDATION, method = RequestMethod.POST)
	public @ResponseBody OtpValidationResponse doOtpValidation(@RequestBody OtpValidationRequest request1) {
		OtpValidationResponse response = new OtpValidationResponse();
		response = registrationService.doOtpValidation(request1);
		return response;

	}
	
	@RequestMapping(value = ApiConstants.USER_MGMT_AUTHENTICATE, method = RequestMethod.POST)
	public @ResponseBody UserAuthenticationResponse authenticateUser(@RequestBody AuthenticateUserRequest request1) {
		UserAuthenticationResponse response = new UserAuthenticationResponse();
		response = registrationService.authenticateUser(request1);
		return response;

	}

	/*@PostMapping(value = ApiConstants.GOOGLE_AUTHENTICATE)
	public @ResponseBody UserAuthenticationResponse authenticateUserByGoogle(@RequestBody AuthenticateUserRequest request1) {
		
		return null;

	}*/
	 /*@RequestMapping(value = ApiConstants.GOOGLE_AUTHENTICATE)
	   public Principal user(Principal principal) {
	      return principal;
	   }*/
	 
	 @RequestMapping(value = ApiConstants.GOOGLE_USER)
     public Principal google_user(Authentication user) {
		 System.out.println("################################################");
		 System.out.println("################################################");
		 System.out.println("################################################");
		 System.out.println("################################################");
		 System.out.println("User"+": "+ user);
		 System.out.println("UserName"+ ":"+user.getName());
		 System.out.println(user.toString());
		 System.out.println(user.getDetails());
		 Object principal =  user.getPrincipal();
		 Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
		 Object credentials = user.getCredentials();
		 Object details = user.getDetails();
		 String name = user.getName();
		 
		 System.out.println(principal);
		 System.out.println(authorities);
		 System.out.println(credentials);
		 System.out.println(details);
		 System.out.println(name);
		 
	       return user;
	   } 
	 
	   
}
