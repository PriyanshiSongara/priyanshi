package com.example.user_mgmt.service;

import com.example.user_mgmt.model.AuthenticateUserRequest;
import com.example.user_mgmt.model.OtpValidationRequest;
import com.example.user_mgmt.model.OtpValidationResponse;
import com.example.user_mgmt.model.RegistrationRequest;
import com.example.user_mgmt.model.RegistrationResponse;
import com.example.user_mgmt.model.UserAuthenticationResponse;

public interface RegistrationService {
		public RegistrationResponse doRegistration(RegistrationRequest registrationRequest);

		public OtpValidationResponse doOtpValidation(OtpValidationRequest request1);

		public UserAuthenticationResponse authenticateUser(AuthenticateUserRequest request1);

}
