package com.example.user_mgmt.serviceImpl;



import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;

import org.springframework.stereotype.Service;

import com.example.user_mgmt.commons.ServiceHelper;
import com.example.user_mgmt.entity.OtpInsertionDto;
import com.example.user_mgmt.entity.RegistraionDto;
import com.example.user_mgmt.entity.UsersDto;
import com.example.user_mgmt.entity.UsersLoginDto;
import com.example.user_mgmt.model.AuthenticateUserRequest;
import com.example.user_mgmt.model.OtpValidationRequest;
import com.example.user_mgmt.model.OtpValidationResponse;
import com.example.user_mgmt.model.RegistrationRequest;
import com.example.user_mgmt.model.RegistrationResponse;
import com.example.user_mgmt.model.UserAuthenticationResponse;
import com.example.user_mgmt.repository.OtpValidationRepository;
import com.example.user_mgmt.repository.RegistrationRepository;
import com.example.user_mgmt.repository.UsersLoginRepository;
import com.example.user_mgmt.repository.UsersRepository;
import com.example.user_mgmt.service.RegistrationService;



@Service("registrationService")
public class RegistrationServiceImpl implements RegistrationService {

	@Autowired
	private RegistrationRepository registrationRepo;
	
	@Autowired
	private OtpValidationRepository otpValidationRepo;
	
	
	@Autowired
	private UsersRepository usersRepo;
	
	@Autowired
	private UsersLoginRepository usersLoginRepo;
	

	@Autowired
	private EntityManager entityManager;
	
	@Autowired
	private ServiceHelper serviceHelper;
	
	
	
	
	RegistraionDto dto=new RegistraionDto();
	OtpInsertionDto otpDto= new OtpInsertionDto();
	UsersDto usersDto =new UsersDto();
	UsersLoginDto usersLoginDto=new UsersLoginDto();
	
	@Override
	public RegistrationResponse doRegistration(RegistrationRequest registrationRequest) {
		Boolean status=Boolean.FALSE;
		
		
		RegistrationResponse response=new RegistrationResponse();
		//ServiceHelper serviceHelper= new ServiceHelper();
		String otp= "";
		EntityManager entity;
		try {
		RegistraionDto byEmail = registrationRepo.getByEmail(registrationRequest.getEmail());
		System.out.println(byEmail);
		
		if(byEmail == null)
		{
			System.out.println("User not found");
			String password = BCrypt.hashpw(registrationRequest.getPassword(), BCrypt.gensalt(12));
			System.out.println("Hashed code "+password );
			dto.setEmail(registrationRequest.getEmail());
			dto.setfName(registrationRequest.getfName());
			dto.setlName(registrationRequest.getlName());
			dto.setPassword(password);
			dto.setPhone(registrationRequest.getPhone());
			dto.setIsValidated(0);
		   System.out.println(dto.getId());
			registrationRepo.save(dto);	
			otp=ServiceHelper.generateOtp();
			System.out.println("OTP "+otp);
			otpDto.setUsersOnboardingId(dto.getId());
			otpDto.setOtp(otp);
			//twist
			otpDto.setExpiryTime((String.valueOf((System.currentTimeMillis()/1000)+120000)));
			System.out.println("TIME IN TABLE "+(String.valueOf((System.currentTimeMillis()/1000)+120000)));
			
			
			response.setMessage("New email Id found,successfully registered");
		
			
			otpValidationRepo.save(otpDto);
					    
		}
		else
		{
			response.setMessage("Existing email id");
		}
		
		response.setResponseCode("2000");
		response.setResponseDescription("Success");
		response.setResponseStatus("Success");
		}
		catch(Exception e)
		{
			response.setResponseCode("1999");
			response.setResponseDescription("Failure");
			response.setResponseStatus("Failure");
		}
		return response;
	}







	@Override
	public OtpValidationResponse doOtpValidation(OtpValidationRequest request1) {
		OtpValidationResponse response= new OtpValidationResponse();
		
		OtpInsertionDto detail=null;
		try {
			Query query = entityManager.createNamedQuery("getOtpValidation", OtpInsertionDto.class).setParameter("email", request1.getEmail());
					
					
					

		detail = (OtpInsertionDto) query.getSingleResult();
			System.out.println("Expiry time"+detail.getExpiryTime());
			System.out.println("id "+detail.getId());
			System.out.println("otp in table "+detail.getOtp());
			System.out.println("onboarding id "+detail.getUsersOnboardingId());
			System.out.println("System time "+System.currentTimeMillis()/1000);
			if(((detail.getOtp()).equalsIgnoreCase( request1.getOtp())))
				{
				if((System.currentTimeMillis()/1000)< Long.parseLong(detail.getExpiryTime()))
				
			     {
				System.out.println("OTP validated");
				dto.setIsValidated(1);
				registrationRepo.save(dto);
				System.out.println("Users onboarding updated");
				RegistraionDto byEmail = registrationRepo.getByEmail(request1.getEmail());
				System.out.println(byEmail);
				usersDto.setfName(byEmail.getfName());
				usersDto.setlName(byEmail.getlName());
				usersDto.setUserName(byEmail.getEmail());
				usersDto.setInactive(0);
				usersDto.setPhone(byEmail.getPhone());
			 
				usersRepo.save(usersDto);
				System.out.println("Inserted into users");
				usersLoginDto.setUserId(usersDto.getId());
				usersLoginDto.setPassword(byEmail.getPassword());
				
				usersLoginRepo.save(usersLoginDto);
				System.out.println("inserted into users_logins");
				
			     
			     }
				response.setResponseCode("2000");
				response.setResponseDescription("Success");
				response.setResponseStatus("Success");
				}

			
		} catch (Exception ex) {
			System.out.println("Error in fetching data");
		}
		return response;
	}







	@Override
	public UserAuthenticationResponse authenticateUser(AuthenticateUserRequest request1) {
		UserAuthenticationResponse response= new UserAuthenticationResponse();
		String token=null;
		try
		{UsersDto getAuthenticationDetails=usersRepo.getByUserName(request1.getUserName());
		if(getAuthenticationDetails==null)
		{
			System.out.println("User not found");
		}
		else
		{
			UsersLoginDto loginDetails=usersLoginRepo.getByUserId(getAuthenticationDetails.getId());
			System.out.println(BCrypt.checkpw(request1.getPasswordEntered(), loginDetails.getPassword()));
			token= serviceHelper.createJWT(request1.getUserName());
			System.out.println("Token "+token);
			
		
		}
		}
		catch (Exception ex) {
			System.out.println("Error in authenticating user");
			response.setAuthToken(null);
			response.setMessage("User Validation failed");
			response.setResponseStatus("Failure");
		}
		response.setAuthToken(token);
		response.setMessage("Successfully logged in");
		response.setResponseStatus("Success");
		
		
		return response;
	}

}
