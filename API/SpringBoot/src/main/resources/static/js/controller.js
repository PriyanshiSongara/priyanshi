'use strict';

// Creating the Angular Controller
app.controller('AppCtrl', function ($http, $scope) {

    // method for getting user details
/*  var getUser = function () {
      $scope.user = {
    		            "name": "Priyanshi",
    		            "userAuthentication" : {
    		            	"details" : {
    		            		"picture" : "http://127.0.0.1:5002/user.jpg"	
    		            	}
    		            }
    		            
    		  };
      //$scope.resource = error;
  };*/

    var getUser = function () {
        $http.get('/google/user').success(function (user) {
            $scope.user = user;
            console.log('Logged User : ', user);
       }).error(function (error) {
            $scope.resource = error;
       });
    };
    getUser();

    // method for logout
    $scope.logout = function () {
        $http.post('/logout').success(function (res) {
            $scope.user = null;
        }).error(function (error) {
            console.log("Logout error : ", error);
        });
    };
});
