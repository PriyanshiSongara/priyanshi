import json
import jwt
import sys

def generatePolicy(principalId, effect, methodArn):
    authResponse = {}
    authResponse['principalId'] = principalId
 
    if effect and methodArn:
        policyDocument = {
            'Version': '2012-10-17',
            'Statement': [
                {
                    'Sid': 'FirstStatement',
                    'Action': 'execute-api:Invoke',
                    'Effect': effect,
                    'Resource': methodArn
                }
            ]
        }
 
        authResponse['policyDocument'] = policyDocument
 
    return authResponse
 
def lambda_handler(event, context):

    try:
        # Get token from Authorization header
        print(event)
        print(event['authorizationToken'])
        token = event['authorizationToken'].split(" ")[1]
        print('token: ' + token)

        # Get user info from token
        usrinfo = jwt.decode(token, 'SECRET_KEY')
        print(usrinfo)
        usrid = usrinfo['sub']
        print('usrid: ' + usrid)

    except:
        print(usrinfo)
        print("Oops! ", sys.exc_info()[0], " occured.")
        return generatePolicy(None, 'Deny', event['methodArn'])
    

    return generatePolicy(usrid, 'Allow', event['methodArn'])


if __name__ == '__main__':

    event = {
        "type": "TOKEN",
        "methodArn": "arn:aws:execute-api:us-east-2:403053794370:332hpjmgr1/prod/POST/dashboard",
        "authorizationToken": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NTQ2Mjc2MzAsImlhdCI6MTU1NDU0MTIyNSwic3ViIjoicG50ZXN0QHBudGVzdC5jb20ifQ.vO8Dx5OwkO7OtmfLYzATcXy6WdPpWY0E7GprNhbBQQU"
    }
    policy = lambda_handler(event, None)
    print(policy)
