import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';


@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	registrationForm = this.fb.group({
		userEmail: [''],
		firstName: [''],
		lastName: [''],
		phoneNmber: [''],
		passcode: ['']   
	});
	constructor(private fb: FormBuilder, private http: HttpClient) { }
	onSubmit() {
		// TODO: Use EventEmitter with form value
		console.warn(this.registrationForm.value);
		let data = this.registrationForm.value;
		let userObj =  {
			"email": data.userEmail,
			"fname": data.firstName,
			"lname": data.lastName,
			"phno": data.phoneNmber,
			"password": data.passcode
		}

		this.http.post("Registration",userObj).subscribe(data=>console.log(data))
	}
	title = 'user-management';
}
